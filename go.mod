module gitlab.com/igor.drozdov/lsif-go-parser

go 1.14

require (
	github.com/alecthomas/chroma v0.7.3
	github.com/dlclark/regexp2 v1.2.0 // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.5.1
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
