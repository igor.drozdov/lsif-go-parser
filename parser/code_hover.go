package parser

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/alecthomas/chroma"
	"github.com/alecthomas/chroma/lexers"
)

type CodeHover struct {
	Value    string `json:"value"`
	Language string `json:"language,omitempty"`
}

func NewCodeHover(content json.RawMessage) CodeHover {
	var codeHover CodeHover
	if err := json.Unmarshal(content, &codeHover); err != nil {
		value := string(content)
		unquotedValue, err := strconv.Unquote(value)
		if err != nil {
			return CodeHover{Value: value}
		}

		return CodeHover{Value: unquotedValue}
	}

	codeHover.Highlight()

	return codeHover
}

func (c *CodeHover) Highlight() {
	var b strings.Builder

	isFirst := true
	for _, line := range c.codeLines() {
		if !isFirst {
			b.WriteString("\n")
		}

		b.WriteString(fmt.Sprintf("<span class=\"line\" lang=\"%s\">", c.Language))

		for _, token := range line {
			if strings.HasPrefix(token.Type.String(), "Keyword") || token.Type == chroma.String || token.Type == chroma.Comment {
				class := chroma.StandardTypes[token.Type]
				value := fmt.Sprintf("<span class=\"%s\">%s</span>", class, escapeSpecialChars(token.Value))
				b.WriteString(value)
			} else {
				b.WriteString(escapeSpecialChars(token.Value))
			}
		}

		isFirst = false

		b.WriteString("</span>")
	}

	c.Value = b.String()
}

func escapeSpecialChars(value string) string {
	if matched, err := regexp.MatchString("[&<>\n]", value); !matched || err != nil {
		return value
	}

	r := strings.NewReplacer("<", "&lt;", ">", "&gt;", "&", "&amp;", "\n", "")
	return r.Replace(value)
}

func (c *CodeHover) codeLines() [][]chroma.Token {
	lexer := lexers.Get(c.Language)
	if lexer == nil {
		return [][]chroma.Token{}
	}

	iterator, err := lexer.Tokenise(nil, c.Value)
	if err != nil {
		return [][]chroma.Token{}
	}

	return chroma.SplitTokensIntoLines(iterator.Tokens())
}
