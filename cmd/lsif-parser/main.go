package main

import (
	"io"
	"log"
	"os"

	"gitlab.com/igor.drozdov/lsif-go-parser/parser"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatal("Please specify filename")
	}

	filePath := os.Args[1]
	file, err := os.Open(filePath)
	defer file.Close()
	if err != nil {
		log.Fatal(err)
	}

	p, err := parser.NewParser(file, "")
	if err != nil {
		log.Fatal(err)
	}

	r, err := p.ZipReader()
	if err != nil {
		log.Fatal(err)
	}

	if err := p.Close(); err != nil {
		log.Fatal(err)
	}

	w, err := os.Create(filePath + ".zip")
	if err != nil {
		log.Fatal(err)
	}
	defer w.Close()

	if _, err := io.Copy(w, r); err != nil {
		log.Fatal(err)
	}
}
